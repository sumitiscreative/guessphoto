//
//  FlickrHandler.h
//  GuessPhoto
//
//  Created by SumitKumar on 29/03/17.
//  Copyright © 2017 SumitKumar. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^myCompletion)(NSMutableArray* finalArray, NSError* error);

@interface FlickrHandler : NSObject

+(void)fetchRandomImagesFromFlicker:(myCompletion)completionBlock;

@end
