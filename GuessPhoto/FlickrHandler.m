//
//  FlickrHandler.m
//  GuessPhoto
//
//  Created by SumitKumar on 29/03/17.
//  Copyright © 2017 SumitKumar. All rights reserved.
//

#import "FlickrHandler.h"

#define kPublicPhoto @"https://api.flickr.com/services/rest/?method=flickr.galleries.getPhotos&api_key=83e93a9112bb4cbea880962e533b0fd0&gallery_id=66911286-72157647277042064&format=json&nojsoncallback=1"

@implementation FlickrHandler


+(void)fetchRandomImagesFromFlicker:(myCompletion)completionBlock{
    
    __block NSMutableArray* imgArr = [NSMutableArray array];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *sesion = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask* dataTask = [sesion dataTaskWithURL:[NSURL URLWithString:kPublicPhoto] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(error){
            completionBlock(nil,error);
        }
        if (!error && data) {
            //NSLog(@"Response  : %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            id obj = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error: &error];
            //NSMutableDictionary *jsonDict = nil;
            if([obj isKindOfClass:[NSDictionary class]]){
                NSDictionary *finalDict = [NSMutableDictionary dictionaryWithDictionary:(NSDictionary*)obj];
                
                for (int i = 0 ; i < 9; i++) {
                    
                    NSDictionary* jsonDict = [[[finalDict objectForKey:@"photos"] objectForKey:@"photo"] objectAtIndex:i];
                    //  https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
                    //https://farm9.staticflickr.com/8187/8432423659_dd1b834ec5.jpg
                    NSString* iPath = [NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@.jpg",
                                       [jsonDict objectForKey:@"farm"],
                                       [jsonDict objectForKey:@"server"],
                                       [jsonDict objectForKey:@"id"],
                                       [jsonDict objectForKey:@"secret"]];
                    NSLog(@"image url %d : %@",i,iPath);
                    [imgArr addObject:iPath];
                    
                }
                completionBlock(imgArr,error);
            }
            
        }
            
        
    }];
    [dataTask resume];
    
}

-(NSString*)getcachepath{
    NSArray* cacheArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, true);
    return [cacheArray lastObject];
}

@end
