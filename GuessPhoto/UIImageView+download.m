//
//  UIImageView+download.m
//  GuessPhoto
//
//  Created by SumitKumar on 29/03/17.
//  Copyright © 2017 SumitKumar. All rights reserved.
//

#import "UIImageView+download.h"

@implementation UIImageView (download)
-(void)downloadThisImage:(NSString*)path1{
    __block NSString* path = path1;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"%@",path);
    //path = @"http://img1.holidayiq.com/imgs/itinerary/medium/shareiq_33_1490084531.395111.jpg";
    NSString *imgPath = [[UIImageView getcachepath] stringByAppendingString:path];
    if([[NSFileManager defaultManager] fileExistsAtPath:imgPath]){
        dispatch_async(dispatch_get_main_queue(), ^{
        self.image = [UIImage imageWithContentsOfFile:imgPath];
        });
    }
    return;
    
    NSURLSession *sesion = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[sesion downloadTaskWithURL:[NSURL URLWithString:path] completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
    
    if (error) {
        [self downloadThisImage:path];
    }
    
    if([[NSFileManager defaultManager] copyItemAtURL:location toURL:[NSURL URLWithString:imgPath] error:nil]){
        dispatch_async(dispatch_get_main_queue(), ^{
            self.image = [UIImage imageWithContentsOfFile:imgPath];
        });
    }else{
        [self downloadThisImage:path];
    }
    }] resume];
        
    });
}

+(NSString*)getcachepath{
    NSArray* cacheArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, true);
    return [cacheArray lastObject];
}


@end
