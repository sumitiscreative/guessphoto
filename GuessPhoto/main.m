//
//  main.m
//  GuessPhoto
//
//  Created by SumitKumar on 26/03/17.
//  Copyright © 2017 SumitKumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
