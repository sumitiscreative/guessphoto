//
//  UIImageView+download.h
//  GuessPhoto
//
//  Created by SumitKumar on 29/03/17.
//  Copyright © 2017 SumitKumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (download)
-(void)downloadThisImage:(NSString*)path;
+(NSString*)getcachepath;
@end
