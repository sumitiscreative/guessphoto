//
//  ViewController.m
//  GuessPhoto
//
//  Created by SumitKumar on 26/03/17.
//  Copyright © 2017 SumitKumar. All rights reserved.
//


// image from flickr
// layour of collectionview
/*
flip no -> show once 
 iddentify -> show permamnet
 can show -> show it
*/
#import "ViewController.h"
#import "UIImageView+download.h"
#import "FlickrHandler.h"
#import "UIImageView+Haneke.h"
#import "GuessPhoto-Bridging-Header.h"
#import "GuessPhoto-Swift.h"

#define kFlickKey @"83e93a9112bb4cbea880962e533b0fd0"
#define kFlickSecret @"d9848ecf397fcb73"

@interface PicView : UICollectionViewCell
@property (nonatomic) IBOutlet UIImageView* imView;
@property (nonatomic) BOOL flipped;
@property (nonatomic) BOOL identified;
@property (nonatomic) NSString *imgUrl;
- (void)FlipImage;
@end

@implementation PicView
-(void)awakeFromNib{
    [super awakeFromNib];
    _flipped = NO;
    _identified = NO;
}

- (void)FlipImage:(BOOL)show withimage:(NSString*)imgString showthis:(BOOL)showNow{
    
        [UIView transitionWithView:_imView
                          duration:1
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                            if(!show || (self.identified && show) || showNow)
                            {
                                [_imView hnk_setImageFromURL:[NSURL URLWithString:imgString]];
                              
                            } else {
                                _imView.image = [UIImage imageNamed:@"placeholder.jpg"];
                              
                            }
                            
                        } completion:nil];
    
}


@end

@interface ViewController () <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    BOOL Flip;
    NSString *curImage;
    NSArray* imgList;
    NSMutableArray* cellNum;
    IBOutlet NSLayoutConstraint* heightView;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* heightView;
@property (weak, nonatomic) IBOutlet UILabel *mesgLbl;
@property (weak, nonatomic) IBOutlet UIImageView *refImage;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSArray* imgarr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[UIImageView getcachepath] error:nil];
    [imgarr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSLog(@"File : %@",obj);
    }];
    
    _mesgLbl.text = @"trying to fetch photos from Flickr";
    [self getDataFromFlickr];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)getDataFromFlickr{
    [FlickrHandler fetchRandomImagesFromFlicker:^(NSMutableArray *finalArray, NSError *error) {
        if(error){
            [self getDataFromFlickr];
            dispatch_async(dispatch_get_main_queue(), ^{
            _mesgLbl.text = @"got some error, now trying again";
            });
        }
        
        if(finalArray && [finalArray count]>0){
            NSLog(@"final : %@",finalArray);
            imgList = [NSArray arrayWithArray:finalArray];
            dispatch_async(dispatch_get_main_queue(), ^{
            _collectView.delegate = self;
            _collectView.dataSource = self;
            _collectView.collectionViewLayout = [[CustomLayout alloc] init];
               
            cellNum = [NSMutableArray arrayWithArray:imgList];
            
            [_collectView reloadData];
            });
            [self startTheTimer];
        }
    }];
}

-(void)startTheTimer{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
    __block NSInteger tCount = 0;
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        _mesgLbl.text = [NSString stringWithFormat:@"Memorize pictures in 15 sec --> %ld secs",(long)++tCount];
        NSLog(@"%@",_mesgLbl.text);
        
        if(tCount==15){
            
            self.refImage.hidden = NO;
            [self selectRandomPic];
            [timer invalidate];
            Flip = YES;
            
            [_collectView reloadData];        
        }
    }];
    });
}


-(void)updateTheStatus{
    dispatch_async(dispatch_get_main_queue(), ^{
    _mesgLbl.text = [NSString stringWithFormat:@"%lu out of 9 indentified",9-[cellNum count]];
    if([cellNum count]==0){
        [self showAlertWithTitle:@"Good" message:@"Game over"];
    }else{
        [self selectRandomPic];
    }
    });
}

-(void)selectRandomPic{
    dispatch_async(dispatch_get_main_queue(), ^{
    NSInteger randomNumber = arc4random() % [cellNum count];
    curImage = [cellNum objectAtIndex:randomNumber];
    [self.refImage hnk_setImageFromURL:[NSURL URLWithString:[cellNum objectAtIndex:randomNumber]]];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [imgList count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PicView* cell = (PicView*)[collectionView cellForItemAtIndexPath:indexPath];
    if(curImage == [imgList objectAtIndex:indexPath.row] && Flip){
        
        //[self showAlertWithTitle:@"Good" message:@"Kudos ! correct answer"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [cellNum removeObject:[imgList objectAtIndex:indexPath.row]];
            cell.identified = YES;
            [cell FlipImage:NO withimage:[imgList objectAtIndex:indexPath.row] showthis:YES];
            [cell setNeedsLayout];
        });
        [self updateTheStatus];
        
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PicView *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PicView" forIndexPath:indexPath];
    cell.imView.tag = indexPath.row;
    cell.imgUrl = [imgList objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell FlipImage:(Flip && !cell.identified) withimage:[imgList objectAtIndex:indexPath.row] showthis:NO];
        [cell setNeedsLayout];
        cell.userInteractionEnabled = Flip;
    });
    
    
    return cell;
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)msg{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
